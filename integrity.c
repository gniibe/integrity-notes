/* SPDX-License-Identifier: LGPL-2.1-or-later */

#define _GNU_SOURCE

#include "integrity.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <gnutls/crypto.h>
#include <link.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define FIPS_KEY "orboDeJITITejsirpADONivirpUkvarP"

#define ALIGN_UP(val, align) (((val) + (align) - 1) & ~((align) - 1))

struct callback_data
{
  char *name;
  int fd;
  void *memory;
  size_t memory_length;
  void *note_start;
  void *note_end;
  gnutls_hmac_hd_t hmac;
};

static inline bool
is_integrity_note (ElfW(Nhdr) *note)
{
  void *name;

  if (!note->n_namesz)
    return false;

  name = (void *)note + sizeof(*note);
  return note->n_namesz == sizeof(FIPS_NOTE_NAME) &&
    !memcmp (name, FIPS_NOTE_NAME, sizeof(FIPS_NOTE_NAME)) &&
    note->n_type == NT_FIPS_INTEGRITY;
}

static int
find_integrity_note_callback (struct dl_phdr_info *info, size_t info_size,
			      void *callback_data)
{
  struct callback_data *data = callback_data;

  if (strcmp (info->dlpi_name, data->name))
    return 0;

  for (int i = 0; i < info->dlpi_phnum; i++)
    {
      void *start = (void *)(data->memory + info->dlpi_phdr[i].p_offset);
      void *end = start + info->dlpi_phdr[i].p_filesz;

      for (void *ptr = start; ptr < end; )
	{
	  ElfW(Nhdr) *note = ptr;
	  size_t size;

	  size = sizeof(*note) +
	    ALIGN_UP(note->n_namesz, 4) +
	    ALIGN_UP(note->n_descsz, 4);

	  if (end - ptr < size)
	    break;

	  if (is_integrity_note (note))
	    {
	      data->note_start = note;
	      data->note_end = data->note_start + size;
	      return 1;
	    }

	  ptr += size;
	}
    }
  return 0;
}

static int
calculate_integrity_callback (struct dl_phdr_info *info, size_t info_size,
			      void *callback_data)
{
  struct callback_data *data = callback_data;

  if (strcmp (info->dlpi_name, data->name))
    return 0;

  for (int i = 0; i < info->dlpi_phnum; i++)
    {
      int ret;

      void *start = (void *)(data->memory + info->dlpi_phdr[i].p_offset);
      void *end = start + info->dlpi_phdr[i].p_filesz;

      /* Empty segment */
      if (start == end)
	continue;

      if (!info->dlpi_phdr[i].p_offset)
	{
	  ElfW(Ehdr) header;

	  /* ELF header must be at the beginning of the file */
	  assert (info->dlpi_phdr[i].p_filesz >= sizeof(ElfW(Ehdr)));

	  /* Clear section information, which is only used at link time */
	  memcpy (&header, start, sizeof(header));
	  header.e_shoff = 0;
	  header.e_shentsize = 0;
	  header.e_shnum = 0;
	  header.e_shstrndx = 0;

	  ret = gnutls_hmac (data->hmac, &header, sizeof(header));
	  assert (!ret);

	  start += sizeof(header);
	}

      /* If the segment contains .note.integrity, skip the region */
      if (start <= data->note_start)
	{
	  /* The note should not span across multiple segments */
	  assert (data->note_end <= end);

	  ret = gnutls_hmac (data->hmac, start, data->note_start - start);
	  assert (!ret);

	  ret = gnutls_hmac (data->hmac, data->note_end, end - data->note_end);
	  assert (!ret);
	}
      else
	{
	  ret = gnutls_hmac (data->hmac, start, end - start);
	  assert (!ret);
	}
    }
  return 0;
}

static inline void
callback_data_deinit (struct callback_data *data)
{
  uint8_t digest[32];

  free (data->name);
  if (data->hmac)
    gnutls_hmac_deinit (data->hmac, digest);
  if (data->memory)
    munmap (data->memory, data->memory_length);
  if (data->fd >= 0)
    close (data->fd);
}

static inline int
callback_data_init (struct callback_data *data,
		    const char *file, const char *name)
{
  memset (data, 0, sizeof(*data));

  data->name = strdup (name);
  if (!data->name)
    goto error;

  data->fd = open (file, O_RDONLY);
  if (data->fd < 0)
    goto error;

  struct stat sb;
  if (fstat (data->fd, &sb) < 0)
    goto error;

  data->memory_length = ALIGN_UP(sb.st_size, sysconf(_SC_PAGE_SIZE));
  data->memory = mmap (NULL, data->memory_length, PROT_READ, MAP_PRIVATE,
		       data->fd, 0);
  if (!data->memory)
    goto error;

  if (gnutls_hmac_init (&data->hmac, GNUTLS_MAC_SHA256,
			FIPS_KEY, sizeof(FIPS_KEY)-1) < 0)
    goto error;

  return 0;

 error:
  callback_data_deinit (data);
  return -1;
}

int
calculate_integrity (const char *file, const char *name, uint8_t digest[32])
{
  struct callback_data data;

  if (callback_data_init (&data, file, name) < 0)
    return -1;

  dl_iterate_phdr (find_integrity_note_callback, &data);
  dl_iterate_phdr (calculate_integrity_callback, &data);

  gnutls_hmac_output (data.hmac, digest);

  callback_data_deinit (&data);
  return 0;
}

bool
check_integrity (const char *file)
{
  uint8_t digest[32];
  bool ok = false;

  struct callback_data data;

  if (callback_data_init (&data, file, file) < 0)
    return -1;

  dl_iterate_phdr (find_integrity_note_callback, &data);
  if (!data.note_start)
    {
      callback_data_deinit (&data);
      return false;
    }

  dl_iterate_phdr (calculate_integrity_callback, &data);

  gnutls_hmac_output (data.hmac, digest);

  ElfW(Nhdr) *note = data.note_start;
  void *desc = (void *)note + sizeof(*note) + ALIGN_UP(note->n_namesz, 4);
  ok = data.note_end - desc == 32 && !memcmp (desc, digest, 32);
  callback_data_deinit (&data);
  return ok;
}
