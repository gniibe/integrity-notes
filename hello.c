#include <stdio.h>
#include <stdlib.h>

extern const char *greeting (void);

int
main (void)
{
  const char *text = greeting ();
  if (!text)
    return EXIT_FAILURE;

  puts (text);
  return EXIT_SUCCESS;
}
